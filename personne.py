class Personne:
    '''Crée une classe personne ayant un nom'''

    def __init__(self, nom):
        self.nom = nom

    def impression(self):
        print(f'{self.nom}')

    def __repr__(self):
        return f'{self.nom}'


        
