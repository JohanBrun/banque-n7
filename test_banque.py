import pytest
from banque import Banque
from personne import Personne

@pytest.fixture
def banque_test():
    return Banque()

@pytest.fixture
def Johan():
    return Personne('Johan')

@pytest.fixture
def Toto():
    return Personne('Toto')

@pytest.fixture
def Tutu():
    return Personne('Tutu')


def test_ouverture_compte_simple(banque_test, Johan):
    compte_johan = banque_test.ouverture_compte_simple(Johan, 50)
    assert repr(compte_johan.titulaire) == 'Johan'

def test_ouverture_compte_simple2(banque_test, Johan):
    banque_test.ouverture_compte_simple(Johan, 50)
    assert repr(banque_test) == "banque : [Johan, 50]"

def test_total_compte_comptes_simples(banque_test, Johan, Toto):
    banque_test.ouverture_compte_simple(Johan,50)
    banque_test.ouverture_compte_simple(Toto,50)
    somme = banque_test.total_compte()
    assert somme == 100

def test_prelevement_frais_comptes_simples(banque_test, Johan, Toto):
    banque_test.ouverture_compte_simple(Johan,50)
    banque_test.ouverture_compte_simple(Toto,100)
    prelevement = banque_test.prelevement_frais(10)
    assert banque_test.comptes[0].solde == 45
    assert banque_test.comptes[1].solde == 90
    assert prelevement == 15

def test_ouverture_compte_courant(banque_test, Johan):
    test = banque_test.ouverture_compte_courant(Johan, 50)
    assert repr(test) == "Johan, 50"

def test_ouverture_compte_courant2(banque_test, Johan):
    banque_test.ouverture_compte_courant(Johan, 50)
    assert repr(banque_test) == "banque : [Johan, 50]"

def test_total_compte_comptes_courant(banque_test, Johan, Toto):
    banque_test.ouverture_compte_courant(Johan,50)
    banque_test.ouverture_compte_courant(Toto,50)
    somme = banque_test.total_compte()
    assert somme == 100

def test_edition_releve(banque_test, Johan, Toto, Tutu):
    compte_johan = banque_test.ouverture_compte_courant(Johan,50)
    compte_toto = banque_test.ouverture_compte_courant(Toto,100)
    compte_tutu = banque_test.ouverture_compte_simple(Tutu,10)
    banque_test.prelevement_frais(10)
    compte_johan.crediter(5)
    compte_toto.debiter(10)
    compte_johan.debiter(8)
    compte_tutu.debiter(20)
    test = Banque.edition_releve(banque_test)
    assert test == ['Johan : historique : [-5.0, 5, -8] solde : 42.0', 'Toto : historique : [-10.0, -10] solde : 80.0']

def test_numero_compte(banque_test, Johan, Toto, Tutu):
    """Test numero de compte attribué par la banque"""
    compte_johan = banque_test.ouverture_compte_courant(Johan,50)
    compte_toto = banque_test.ouverture_compte_courant(Toto,100)
    compte_tutu = banque_test.ouverture_compte_simple(Tutu,10)
    assert compte_johan.numero_compte == 10001
    assert compte_toto.numero_compte == 10002
    assert compte_tutu.numero_compte == 10003
       
def test_numero_compte2(banque_test, Johan, Toto, Tutu):
    """Test numero de compte attribué par les comptes"""
    compte_johan = banque_test.ouverture_compte_courant(Johan,50)
    compte_toto = banque_test.ouverture_compte_courant(Toto,100)
    compte_tutu = banque_test.ouverture_compte_simple(Tutu,10)
    assert compte_johan.num_cpt == 10017
    assert compte_toto.num_cpt == 10018
    assert compte_tutu.num_cpt == 10019