import pytest
from compte import CompteCourant, CompteSimple

@pytest.fixture
def cpteA():
    return CompteSimple('Johan', 10)

def test_compte_simple(cpteA):
    assert cpteA.titulaire == "Johan"
    assert cpteA.solde == 10

def test_crediter_simple(cpteA):
    cpteA.crediter(10)
    assert cpteA.solde == 20

def test_debiter_simple(cpteA):
    cpteA.debiter(5)
    assert cpteA.solde == 5

@pytest.fixture
def cpteB():
    return CompteCourant('Toto', 10)

def test_compte_courant(cpteB):
    assert cpteB.titulaire == "Toto"
    assert cpteB.solde == 10

def test_crediter_courant(cpteB):
    cpteB.crediter(10)
    assert cpteB.solde == 20

def test_debiter_courant(cpteB):
    cpteB.debiter(5)
    assert cpteB.solde == 5

def test_historique(cpteB):
    assert cpteB.historique == []

def test_historique_mouvement(cpteB):
    cpteB.crediter(8)
    cpteB.debiter(3)
    assert cpteB.historique == [8,-3]
