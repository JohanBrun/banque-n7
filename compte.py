from personne import Personne

class CompteSimple:
    '''Creation d'un compte simple identifié par son titulaire et son solde.
    Possibilité de le créditer ou de le débiter.
    variable :
        titulaire : classe Personne
        solde : number
    '''
    num_cpt = 10001

    def __init__(self, titulaire, solde):
        self.titulaire = titulaire
        self.__solde = solde
        self.numero_compte = 0
        self.num_cpt = CompteSimple.num_cpt
        CompteSimple.num_cpt+=1
    @property
    def solde(self):
        return self.__solde

    @solde.setter
    def solde(self, value):
        self.__solde = value

    def crediter(self, credit):
        self.__solde += credit

    def debiter(self, debit):
        self.__solde -= debit

   
    def __repr__(self):
        return '{}, {}'.format(self.titulaire, self.__solde)




class CompteCourant(CompteSimple):
    '''Création d'un compte courant identifié par son titulaire et son solde.
    Possibilité de le créditer, de le débiter, et d'éditer un relevé
    variable:
        titulaire : classe Personne
        solde : number
    '''

    def __init__(self, titulaire, solde):
        super().__init__(titulaire, solde)
        self.historique = []
        

    def crediter(self, credit):
        super().crediter(credit)
        self.historique.append(credit)

    def debiter(self, debit):
        super().debiter(debit)
        self.historique.append(-debit)

    def edition_releve(self,):
        return self.historique, self.solde

    def __str__(self):
        return f"{self.titulaire} a {self.solde}€ sur son compte en banque." 
   

    def __repr__(self):
        return f'{self.titulaire}, {self.solde}'
