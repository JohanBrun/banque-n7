from compte import CompteSimple, CompteCourant
from personne import Personne


class Banque:
    '''Création d'une Banque identifiée par sa liste de comptes. 
    Possibilité d'ouvrir un compte simple ou courant, d'obtenir le total des sommes
    gérées par la banque, d'éditer la liste des relevés de compte
    variable :
        None
    '''
 
    

    def __init__(self):
        self.comptes = []
        self.numero_comptes = 10001

    def ouverture_compte_simple(self, titulaire, depot_initial):
        """Ouvre un compte de type simple"""
        compte = CompteSimple(titulaire, depot_initial)
        self.comptes.append(compte)
        compte.numero_compte = self.numero_comptes
        self.numero_comptes+=1
        return compte

    def ouverture_compte_courant(self, titulaire, depot_initial):
        """Ouvre un compte de type courant"""
        compte_courant = CompteCourant(titulaire, depot_initial)
        self.comptes.append(compte_courant)
        compte_courant.numero_compte = self.numero_comptes
        self.numero_comptes+=1
        return compte_courant
        
    
    def total_compte(self):
        """Donne le total des sommes sur les comptes de la Banque"""
        somme_comptes = 0
        for compte in self.comptes:
            somme_comptes += compte.solde
        return somme_comptes

    def prelevement_frais(self, pourcentage_frais):
        """Prélève un % de frais sur l'ensemble des comptes de la banque et retourne le montant
        total prélevé"""
        total_frais = 0
        for compte in self.comptes:
            montant_frais = (compte.solde * (pourcentage_frais/100))
            compte.debiter(montant_frais)
            total_frais += montant_frais
        return total_frais

    def edition_releve(self):
        """Retourne une liste de l'ensemble des titulaires de compte courant
         avec leur historique et leur solde"""
        releve_banque = []
        for compte in self.comptes:
            if isinstance(compte, CompteCourant):
                releve_banque.append(f"{compte.titulaire} : historique : {compte.historique} solde : {compte.solde}")
        return releve_banque
    

    def __repr__(self):
        return f"banque : {self.comptes}"

    def __str__(self):
        return f"comptes : {self.comptes}"

